package com.hasanzada.bookstore.controller;

import com.hasanzada.bookstore.dto.BookDto;
import com.hasanzada.bookstore.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/book")
@RequiredArgsConstructor
public class BookController {
    private final BookService bookService;
    @GetMapping
    public List<BookDto> getAllBooks() {
        return bookService.getAllBooks();
    }
    @PostMapping("/addBook")
    public BookDto addBook(@RequestBody BookDto bookDto) {
        return bookService.addBook(bookDto);
    }
}
