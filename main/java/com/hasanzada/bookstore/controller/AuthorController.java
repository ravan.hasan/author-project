package com.hasanzada.bookstore.controller;

import com.hasanzada.bookstore.dto.AuthorDto;
import com.hasanzada.bookstore.exception.AuthorNotFoundException;
import com.hasanzada.bookstore.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/author")
@RequiredArgsConstructor
public class AuthorController {
    private final AuthorService authorService;

    @GetMapping
    public List<AuthorDto> getAllAuthors() {
        return authorService.getAllAuthor();
    }

    @PostMapping("/addAuthor")
    public AuthorDto addAuthor(@RequestBody AuthorDto authorDto) {
        return authorService.addAuthor(authorDto);
    }

    @GetMapping("/getAuthor/{id}")
    public AuthorDto getAuthorBuyId(@PathVariable Long id) {
        return authorService.getAuthorById(id);
    }

    @PutMapping("/updateAuthor/{id}")
    public AuthorDto updateAuthor(@PathVariable Long id, @RequestBody AuthorDto authorDto) throws AuthorNotFoundException {
        return authorService.updateDto(id, authorDto);
    }

    @DeleteMapping("/deleteAuthor/{id}")
    public AuthorDto deleteAuthor(@PathVariable Long id, @RequestBody AuthorDto authorDto) throws AuthorNotFoundException {
        return authorService.deleteAuthor(id, authorDto);
    }
}
