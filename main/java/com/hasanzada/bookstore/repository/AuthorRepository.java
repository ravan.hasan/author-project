package com.hasanzada.bookstore.repository;

import com.hasanzada.bookstore.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AuthorRepository extends JpaRepository<Author, Long> {
//    @Query("SELECT a FROM Author a JOIN FETCH a.books")
//    List<Author> getAllAuthor();
}
