package com.hasanzada.bookstore.repository;

import com.hasanzada.bookstore.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
