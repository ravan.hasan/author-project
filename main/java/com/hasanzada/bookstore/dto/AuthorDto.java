package com.hasanzada.bookstore.dto;

import com.hasanzada.bookstore.model.Book;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AuthorDto implements Serializable {
    private String name;
    private List<BookDto> books;
}
