package com.hasanzada.bookstore.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BookDto  implements Serializable {
    private String name;
}
