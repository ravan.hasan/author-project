package com.hasanzada.bookstore.mapper;

import com.hasanzada.bookstore.dto.BookDto;
import com.hasanzada.bookstore.model.Book;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public abstract class BookMapper {
    public static BookMapper INSTANCE = Mappers.getMapper(BookMapper.class);
    public abstract List<BookDto> mapBookEntitiesToBookDtos(List<Book> books);
    public abstract BookDto mapBookEntityToBookDto(Book book);
    public abstract Book mapBookDtoToBook(BookDto bookDto);
    public abstract List<Book> mapBookDtosToBookEntites(List<BookDto> bookDtos);
}
