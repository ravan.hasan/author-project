package com.hasanzada.bookstore.mapper;

import com.hasanzada.bookstore.dto.AuthorDto;
import com.hasanzada.bookstore.model.Author;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public abstract class AuthorMapper {
    public static AuthorMapper INSTANCE = Mappers.getMapper(AuthorMapper.class);

    public abstract List<AuthorDto> mapAuthorEntitiesToAuthorDtos(List<Author> authors);

    public abstract AuthorDto mapAuthorEntityToAuthorDto(Author author);
    public abstract Author mapAuthorDtoToAuthor(AuthorDto authorDto);
}
