package com.hasanzada.bookstore.service;

import com.hasanzada.bookstore.dto.AuthorDto;
import com.hasanzada.bookstore.exception.AuthorNotFoundException;
import com.hasanzada.bookstore.mapper.AuthorMapper;
import com.hasanzada.bookstore.mapper.BookMapper;
import com.hasanzada.bookstore.model.Author;
import com.hasanzada.bookstore.repository.AuthorRepository;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;


    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Cacheable(cacheNames = "allAuthorCache")
    public List<AuthorDto> getAllAuthor() {
        return AuthorMapper.INSTANCE.mapAuthorEntitiesToAuthorDtos(authorRepository.findAll());
    }

    @Cacheable(cacheNames = "myCache")
    public AuthorDto getAuthorById(Long id) {
        return AuthorMapper.INSTANCE.mapAuthorEntityToAuthorDto(authorRepository.findById(id).get());
    }

    @CacheEvict(cacheNames = "myCache", key = "#id")
    public AuthorDto updateDto(Long id, AuthorDto authorDto) throws AuthorNotFoundException {
        Author author = authorRepository.findById(id).orElseThrow(() -> new AuthorNotFoundException("Author Not Found With This ID")
        );
        author.setBooks(BookMapper.INSTANCE.mapBookDtosToBookEntites(authorDto.getBooks()));
        author.setName(authorDto.getName());
        authorRepository.save(author);
        return AuthorMapper.INSTANCE.mapAuthorEntityToAuthorDto(author);
    }

    public AuthorDto addAuthor(AuthorDto authorDto) {
        Author author = authorRepository.save(AuthorMapper.INSTANCE.mapAuthorDtoToAuthor(authorDto));
        return AuthorMapper.INSTANCE.mapAuthorEntityToAuthorDto(author);
    }

    public AuthorDto deleteAuthor(Long id, AuthorDto authorDto) throws AuthorNotFoundException {
        Author author = authorRepository.findById(id).orElseThrow(() -> new AuthorNotFoundException("Author Not Found With This ID"));
        author.setName(null);
        author.setBooks(null);
        authorRepository.save(author);
        return AuthorMapper.INSTANCE.mapAuthorEntityToAuthorDto(author);
    }
}
