package com.hasanzada.bookstore.service;

import com.hasanzada.bookstore.dto.BookDto;
import com.hasanzada.bookstore.mapper.BookMapper;
import com.hasanzada.bookstore.model.Book;
import com.hasanzada.bookstore.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    private final BookRepository bookRepository;

    public BookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<BookDto> getAllBooks() {
        return BookMapper.INSTANCE.mapBookEntitiesToBookDtos(bookRepository.findAll());
    }

    public BookDto addBook(BookDto bookDto) {
        Book book = bookRepository.save(BookMapper.INSTANCE.mapBookDtoToBook(bookDto));
        return BookMapper.INSTANCE.mapBookEntityToBookDto(book);
    }
}
